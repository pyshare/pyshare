# pyShare

## prerequisite

- [docker compose](https://docs.docker.com/compose/)  

## notice

the repo url of submodules are ssh form, switch to https ones if you need.  

## memo

### Clone & Update submodules to master

`git clone --recurse-submodules git@gitlab.com:pyshare/pyshare.git`  
`cd pyshare`  
`git submodule foreach --recursive git checkout master`  

### Run all services

`docker-compose up -d`  

or if you wanna rebuild  

`docker-compose up --build -d`

### 備份使用方法
1. 以下的操作皆是在 backup 資料夾內。此程式可用 NAS 或 Google Drive 來備份，可用 Discord 或 Slack 來通知。
2. 使用 .env.example 來產生 .env ：
    - `SLACK_APP_TOKEN` 的來源為 Slack app ，要在 OAuth & Permissions 的 tab 設定，他會在 OAuth Tokens for Your Workspace 的欄位中產生。（如用 Slack 才需要）
    - `CHANNEL_ID` 可以改成其他頻道 ID ， Slack 網頁版中一個頻道的網址最後一個字串即為它的 ID。（如用 Slack 才需要）
    - `FOLDER_ID` 可以改成其他資料夾 ID ，你可以使用 backup.py 內的 `get_files()` 取得你雲端硬碟中根目錄所有檔案的 ID。（如用 Google Drive 才需要）
    - `DISCORD_WEBHOOK` 可以改成其他 webhook url ，可在 Discord 的伺服器設定中的整合 tab 創建 webhook。（如用 Discord 才需要）
3. 目前程式是使用 NAS 來備份 。如要切換，只要在 `if __name__ == '__main__':` 中將 `NAS_backup()` 替換成 `backup()` 即可。如使用 Google Drive 請看 4, 5 點，如用 NAS 請看 6 點。7, 8, 9 點兩者都要看。
4. 首先根據這篇文章：https://medium.com/@newlife617/%E7%A8%8B%E5%BC%8F-python-google-drive-api-eebeb58876ef，開通 Google API ，得到 client_secrets.json 。
5. 執行 `python3 backup.py once` 來備份一次並生出 mycreds.json ，這樣之後就不用再手動認證。 
6. 如使用 NAS 備份，需在 `NAS_backup()` 中設定 NAS 的帳號、網址、資料夾。此外也要確定執行此程式的主機能夠在不輸入密碼的情況下 ssh 到 NAS 的主機，作法為將執行此程式的主機的 ssh 公鑰放到 NAS 的主機。此專案的 Dockerfile 會將主機的私鑰和 known_hosts 帶到 container 裡。
7. 目前程式是用 Discord 來備份，如要切換，將程式中的 `update_discord()` 改成 `update_slack()` 即可
8. backup.py 的使用方法為 `python3 backup.py [once|always|slack_test|discord_test]` ，功能分別為備份一次，定期備份，測試 Slack 訊息，測試 Discord 訊息。
9. 上述都完成後，只要使用 `docker-compose up -d` ，就能使用定期備份功能。

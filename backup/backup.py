from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from pydrive.files import GoogleDriveFile
from pydrive.files import MediaIoBaseUpload
from apscheduler.schedulers.blocking import BlockingScheduler
from datetime import datetime
import shutil
import os
from dotenv import load_dotenv
import requests
import sys
from discord import SyncWebhook
from subprocess import check_output

load_dotenv()
TOKEN = os.getenv('SLACK_APP_TOKEN')
CHANNEL_ID = os.getenv('CHANNEL_ID')
FOLDER_ID = os.getenv('FOLDER_ID')
DISCORD_WEBHOOK = os.getenv('DISCORD_WEBHOOK')


def _BuildMediaBody(self):
    """Build MediaIoBaseUpload to get prepared to upload content of the file.
    Sets mimeType as 'application/octet-stream' if not specified.
    :returns: MediaIoBaseUpload -- instance that will be used to upload content.
    """
    if self.get('mimeType') is None:
        self['mimeType'] = 'application/octet-stream'
    return MediaIoBaseUpload(
        self.content,
        self['mimeType'],
        resumable=True,
        chunksize=1024 * 1024 * 1024 * 1024,
    )


GoogleDriveFile._BuildMediaBody = _BuildMediaBody


def backup(drive):
    time_msg = datetime.now()
    print(f'Uploaded: {time_msg}')
    shutil.make_archive('MongoDB', 'zip', 'MongoDB')
    file_drive = drive.CreateFile({
        'parents': [{
            'id': FOLDER_ID
        }],
        'title': f'{time_msg}.zip'
    })
    file_drive.SetContentFile("MongoDB.zip")
    print(file_drive.Upload())
    update_discord(f'pyShare 上次上傳時間：{time_msg}')


def NAS_backup():
    time_msg = str(datetime.now()).replace(' ', '_')
    print(f'Uploaded: {time_msg}')
    shutil.make_archive('MongoDB', 'zip', 'MongoDB')

    result = check_output(
        f'rsync -avx MongoDB.zip python@192.168.50.7:/volume1/pyshare/backup/{time_msg}.zip',
        shell=True)
    print(result)
    update_discord(f'pyShare 上次上傳時間：{time_msg}')


def get_files(drive):
    file_list = drive.ListFile({
        'q': "'root' in parents and trashed=false"
    }).GetList()
    for file1 in file_list:
        print('title: %s, id: %s' % (file1['title'], file1['id']))


def update_slack(msg):
    headers = {'Authorization': f'Bearer {TOKEN}'}
    params = {'channel': CHANNEL_ID, 'text': msg, 'pretty': '1'}
    r = requests.get(
        'https://slack.com/api/chat.postMessage',
        headers=headers,
        params=params,
    )
    return r


def update_discord(msg):
    webhook = SyncWebhook.from_url(DISCORD_WEBHOOK)
    webhook.send(msg)


def get_drive():
    g_login = GoogleAuth()
    g_login.LoadCredentialsFile("mycreds.json")
    if g_login.credentials is None:
        g_login.CommandLineAuth()
    elif g_login.access_token_expired:
        g_login.Refresh()
    else:
        g_login.Authorize()
    g_login.SaveCredentialsFile("mycreds.json")
    return GoogleDrive(g_login)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[1] == 'once':
            NAS_backup()
        elif sys.argv[1] == 'always':
            sched = BlockingScheduler()
            sched.add_job(lambda: NAS_backup(), 'cron', hour=20)
            sched.start()
        elif sys.argv[1] == 'slack_test':
            r = update_slack('此為測試訊息')
            print(f'執行結果：\n{r.text}')
        elif sys.argv[1] == 'discord_test':
            update_discord('此為測試訊息')
        else:
            print('python3 backup.py [once|always|slack_test|discord_test]')
    else:
        print('python3 backup.py [once|always|slack_test|discord_test]')

rm ./frontend/.env.production.local
echo "VITE_APP_GIT_SHA=$(git --git-dir=./frontend/.git rev-parse --short=8 HEAD)" > ./frontend/.env.production.local
docker-compose -f docker-compose.yml -f docker-compose.prod.yml build --no-cache
docker-compose down
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
